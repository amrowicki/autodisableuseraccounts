. $PSScriptRoot\Out-Log.ps1
function Set-UserAccount
{
    [CmdletBinding(SupportsShouldProcess = $True)]
    [OutputType([Void])]

    Param(
        [Parameter(Mandatory = $true)]
        [Microsoft.ActiveDirectory.Management.ADUser[]]
        $Users,

        [Parameter(Mandatory = $true)]
        [String]
        $Server,

        [Parameter(Mandatory = $true)]
        [Int]
        $DaysInactive,

        [Parameter(Mandatory = $true)]
        [String]
        $NewOU
    )

    Foreach ($User in $Users)
    {
        if ($PSCmdlet.ShouldProcess($User, "Setting account"))
        {
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Disabling $($User.samaccountname) account"
            try
            {
                Disable-ADAccount $User -Server $Server -Confirm:$false
            }
            catch
            {
                Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "ERROR"
            }

            $Description = (Get-ADUser -Identity $User -Server $Server -Properties Description).description
            $NewDescription = "Konto wylaczone automatycznie w dniu " + (Get-Date -DisplayHint Date -Format yyyy-MM-dd) + " o godzinie " + (Get-Date -DisplayHint Time -Format HH:m) + " z powodu braku aktywnosci od " + $DaysInactive + " dni."
            if ([string]::IsNullOrEmpty($Description))
            {
                $MergedDescription = $NewDescription
            }
            else
            {
                $MergedDescription = "$Description; $NewDescription"
            }
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Setting description"
            try
            {
                Set-ADUser $User -Server $Server -Description $MergedDescription -Confirm:$false
            }
            catch
            {
                Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "ERROR"
            }

            foreach ($group in $User.memberof)
            {
                if ((Get-ADGroup $group -Server $Server).GroupCategory -eq 'Distribution')
                {
                    Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Removing from '$((Get-ADGroup $group -Server $Server).Name)' distribution group"
                    try
                    {
                        Remove-ADGroupMember -Identity $group -Members $User -Server $Server -Confirm:$false
                    }
                    catch
                    {
                        Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "ERROR"
                    }
                }
            }

            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Moving user to new OU: $OU"
            try
            {
                Move-ADObject $User -TargetPath $NewOU -Server $Server -Confirm:$false
            }
            catch
            {
                Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "ERROR"
            }
        }
    }
}