. $PSScriptRoot\Set-GetADUserFilter.ps1
. $PSScriptRoot\Add-RequiredTypeData.ps1
Write-Verbose 'Set-GetADUserProperties'

function Set-GetADUserProperties
{
    [CmdletBinding()]
    [OutputType([System.Collections.Hashtable])]
    param(

        [Parameter(Mandatory = $true)]
        [String]
        $SearchBase,

        [Parameter(Mandatory = $true)]
        [String]
        $Server,

        [Parameter()]
        [System.Nullable[Int]]
        $ResultSetSize,

        [Parameter(Mandatory = $true)]
        [System.Int64]
        $lastLogonIntervalLimit,

        [Parameter()]
        [String[]]
        $ExcludedAccounts,

        [Parameter()]
        [String[]]
        $ExcludedGroups,

        [Parameter(Mandatory = $true)]
        [Int]
        $SearchScope,

        [Parameter()]
        [System.Collections.ArrayList]
        $IncludedProperties = @()
    )

    $Parameters = @{
        SearchBase  = $SearchBase
        Server      = $Server
        Filter      = Set-GetADUserFilter -lastLogonIntervalLimit $lastLogonIntervalLimit -ExcludedAccounts $ExcludedAccounts -ExcludedGroups $ExcludedGroups
        SearchScope = $SearchScope
    }

    if ($IncludedProperties.Contains('Parent'))
    {
        $IncludedProperties.Remove('Parent') | Out-Null
        Add-RequiredTypeData -IncludeParent -Server $Server
    }
    else
    {
        Add-RequiredTypeData -Server $Server
    }

    if (-not $IncludedProperties.Contains('Name')) {$IncludedProperties.add('Name') | Out-Null}
    if (-not $IncludedProperties.Contains('samAccountName')) {$IncludedProperties.add('samAccountName') | Out-Null}
    if (-not $IncludedProperties.Contains('PasswordNeverExpires')) {$IncludedProperties.add('PasswordNeverExpires') | Out-Null}
    if (-not $IncludedProperties.Contains('lastlogontimestamp')) {$IncludedProperties.add('lastlogontimestamp') | Out-Null}
    if (-not $IncludedProperties.Contains('memberof')) {$IncludedProperties.add('memberof') | Out-Null}
    if (-not $IncludedProperties.Contains('Created')) {$IncludedProperties.add('Created') | Out-Null}
    if (-not $IncludedProperties.Contains('PasswordLastSet')) {$IncludedProperties.add('PasswordLastSet') | Out-Null}
    $Parameters.Add('Properties', $IncludedProperties)

    if ($ResultSetSize -ne $null) {$Parameters.Add('ResultSetSize', $ResultSetSize)}

    return $Parameters
}