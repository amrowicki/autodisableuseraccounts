. $PSScriptRoot\Get-LastLogon.ps1
Write-Verbose 'Add-RequiredTypeData'

function Add-RequiredTypeData
{
    [CmdletBinding()]
    [OutputType([Void])]
    param(
        [Parameter()]
        [Switch]
        $IncludeParent,

        [Parameter()]
        [string]
        $Server
    )
    if ($IncludeParent -and ((Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -notcontains 'Parent'))
    {
        Update-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser -MemberName Parent -MemberType ScriptProperty -Value {
            $($this.DistinguishedName -split '(?<![\\]),')[1..$(($this.DistinguishedName -split '(?<![\\]),').Count - 1)] -join ','
        }
    }

    $Global:Server = $Server

    if ((Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -notcontains 'TrueLastLogonTimestamp')
    {
        Update-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser -MemberName TrueLastLogonTimestamp -MemberType ScriptProperty -Value {
            (Get-LastLogon $this.samAccountName -Server $Global:Server)[0]
        }
    }

    if ((Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -notcontains 'TrueLastLogonTimestampDateTime')
    {
        Update-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser -MemberName TrueLastLogonTimestampDateTime -MemberType ScriptProperty -Value {
            (Get-LastLogon $this.samAccountName -Server $Global:Server)[1]
        }
    }

    if ((Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -notcontains 'LastLogonTimestampDateTime')
    {
        Update-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser -MemberName LastLogonTimestampDateTime -MemberType ScriptProperty -Value {
            [DateTime]::FromFileTime((Get-ADUser $this.samAccountName -Server $Global:Server -Properties lastlogontimestamp).lastlogontimestamp)
        }
    }
}