function Set-MailBody
{
    return "#MAXIMO_EMAIL_BEGIN
        LSNRACTION=CREATE
        ;
        LSNRAPPLIESTO=SR
        ;
        EXTERNALSYSTEM=EMAIL
        ;
        TICKETID=&AUTOKEY&
        ;
        CLASS=SR
        ;
        COMMODITY=CLOSEACC
        ;
        CINUM=ACTIVE DIRECTORY CI
        ;
        PLUSPCUSTOMER=Type2
        ;
        OWNERGROUP=group
        ;
        URGENCY=3
        ;
        IMPACT=3
        ;
        DESCRIPTION=Podsumowanie operacji wylaczania kont domenowych
        ;
        DESCRIPTION_LONGDESCRIPTION=
        ;
        #MAXIMO_EMAIL_END
        "
}