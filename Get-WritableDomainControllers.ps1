. $PSScriptRoot\Out-Log.ps1
Write-Verbose 'Get-WritableDomainControllers'

function Get-WritableDomainControllers
{
    [CmdletBinding()]
    [OutputType([String[]])]

    param(
        [Parameter(Mandatory = $true)]
        [String]
        $Server
    )

    $DomainControllers = (Get-ADDomainController -Filter * -Server $Server | Where-Object IsReadonly -eq $false).hostname
    #Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Domain controllers: $DomainControllers"
    return $DomainControllers
}