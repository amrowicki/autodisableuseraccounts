. $PSScriptRoot\Set-GetADUserProperties.ps1
. $PSScriptRoot\Out-Log.ps1
Write-Verbose 'Get-InactiveUsers'
function Get-InactiveUsers
{
    [CmdletBinding()]
    [OutputType([Microsoft.ActiveDirectory.Management.ADUser[]])]

    Param(
        [Parameter()]
        [String]
        $Domain = (Get-ADDomain).DNSRoot,

        [Parameter()]
        [String]
        $Server,

        [Parameter()]
        [String]
        $SearchBase,

        [Parameter()]
        [Int]
        $SearchScope = 2,

        [Parameter()]
        [System.Nullable[Int]]
        $ResultSetSize,

        [Parameter()]
        [Int]
        $DaysInactive = 30,

        [Parameter()]
        [String[]]
        $ExcludedAccounts,

        [Parameter()]
        [String[]]
        $ExcludedGroups,

        [Parameter()]
        [System.Collections.ArrayList]
        $IncludedProperties = @()
    )

    Begin
    {
        if ([string]::IsNullOrEmpty($Server))
        {
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Server not specified"
            $Server = $(Get-ADDomainController -Discover -Service PrimaryDC -DomainName $Domain).hostname
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Setting server to: $Server"
        }
        if ([string]::IsNullOrEmpty($SearchBase))
        {
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "SearchBase not specified"
            $SearchBase = (Get-ADDomain -Server $Server).DistinguishedName
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Setting server to: $SearchBase"
        }
    }

    Process
    {
        $ParamsSetGetADUserProperties = @{
            SearchScope            = $SearchScope
            ResultSetSize          = $ResultSetSize
            SearchBase             = $SearchBase
            Server                 = $Server 
            lastLogonIntervalLimit = $(Get-Date).AddDays( - $DaysInactive).ToFileTime()
            ExcludedAccounts       = $ExcludedAccounts
            ExcludedGroups         = $ExcludedGroups 
            IncludedProperties     = $IncludedProperties
        }

        $GetADUsersParameters = Set-GetADUserProperties @ParamsSetGetADUserProperties
        Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Filter: $($GetADUsersParameters.filter)"
        $Users = Get-ADUser @GetADUsersParameters
        Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "User count from filter: $($Users.count)"
        $Users = $Users | Where-Object {$_.TrueLastLogonTimestamp -le $ParamsSetGetADUserProperties.lastLogonIntervalLimit}
        $Users = foreach ($User in $Users)
        {
            $DirectoryContext = [System.DirectoryServices.ActiveDirectory.DirectoryContext]::new("Domain", $Domain)
            $DomainController = [System.DirectoryServices.ActiveDirectory.DomainController]::FindOne($DirectoryContext)
            $pwdLastSetTime = $DomainController.GetReplicationMetadata($User.DistinguishedName).pwdlastset.LastOriginatingChangeTime
            if ($pwdLastSetTime.ToFileTime() -le $ParamsSetGetADUserProperties.lastLogonIntervalLimit)
            {
                $User
            }
        }
        if ($Users.count -ne 0)
        {
            Out-Log -LogType Type1 -OutputFormat csv -Encoding UTF8 -Message $Users
            Out-Log -LogType Type2 -OutputFormat csv -Encoding UTF8 -Message ($Users | Select-Object Name, SamAccountName, Created, TrueLastLogonTimestampDateTime)
        }
        Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "User count from TrueLastLogon and pwdlastset Last Originating Change Time filter: $($users.count)"
        return $Users
    }
}