﻿. $PSScriptRoot\Get-WritableDomainControllers.ps1
. $PSScriptRoot\Out-Log.ps1
Write-Verbose 'Get-LastLogon'
function Get-LastLogon
{
    [CmdletBinding()]
    [OutputType([System.Collections.ArrayList])]

    param(
        [Parameter(Mandatory = $true)]
        [String]
        $SamAccountName,

        [Parameter(Mandatory = $true)]
        [String]
        $Server
    )

    $DomainControllers = Get-WritableDomainControllers -Server $Server
    $LastLogon = $null

    foreach ($DomainController in $DomainControllers)
    {
        try
        {
            #Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Retrieving user $SamAccountName login data from a $DomainController server"
            $user = Get-ADUser -Identity $SamAccountName -Server $DomainController -Properties LastLogon
            if ($user.LastLogon -gt $LastLogon)
            {
                #Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "New LastLogon value is $($user.LastLogon)"
                $LastLogon = $user.LastLogon
            }
        }
        catch
        {
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "ERROR while retrieving user $SamAccountName last logon date on $($DomainController): $Error[0]"
        }
    }
    return @([System.Int64]$LastLogon, [DateTime]::FromFileTime($LastLogon))
}