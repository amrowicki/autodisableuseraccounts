Write-Verbose 'Set-GetADUserFilter'
function Set-GetADUserFilter
{
    [CmdletBinding()]
    [OutputType([String])]
    param(
        [Parameter(Mandatory = $true)]
        [System.Int64]
        $lastLogonIntervalLimit,

        [Parameter()]
        [String[]]
        $ExcludedAccounts,

        [Parameter()]
        [String[]]
        $ExcludedGroups
    )

    $filter = "(lastlogontimestamp -le $lastLogonIntervalLimit -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimit)).datetime)' -and PasswordLastSet -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimit)).datetime)'"

    if ($ExcludedAccounts -ne $null)
    {
        foreach ($ExcludedAccount in $ExcludedAccounts)
        {
            $filter += " -and samAccountname -ne '$ExcludedAccount'"
        }
    }

    if ($ExcludedGroups -ne $null)
    {
        foreach ($ExcludedGroup in $ExcludedGroups)
        {
            $filter += " -and memberof -notlike '$ExcludedGroup'"
        }
    }
    
    return $filter
}