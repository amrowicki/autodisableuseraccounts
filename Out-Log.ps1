function Out-Log
{
    [CmdletBinding()]
    [OutputType([Void])]

    param(
        [Parameter(Mandatory = $true)]
        [ValidateSet("Type1", "Type2")]
        [String]
        $LogType,

        [Parameter(Mandatory = $true)]
        [ValidateSet("log", "csv")]
        [String]
        $OutputFormat,

        [Parameter(Mandatory = $true)]
        [String]
        $Encoding,

        [Parameter(Mandatory = $true)]
        $Message
    )

    $OutFile = "$PSScriptRoot\Logs\Podsumowanie_"
    if ($LogType -eq 'Type1')
    {
        $OutFile += 'Type1_'
    }
    $OutFile += "$(get-date -f dd-MM-yyyy).$OutputFormat"

    if ($OutputFormat -eq 'log')
    {
        Out-File -FilePath $OutFile -Append -InputObject $Message -Encoding $Encoding
        Write-Verbose $Message
    }
    elseif ($OutputFormat -eq 'csv')
    {
        $Message | Export-Csv -NoTypeInformation -Path $OutFile -Append -Encoding $Encoding
    }
}