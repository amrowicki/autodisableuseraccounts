. $PSScriptRoot\Get-InactiveUsers.ps1
. $PSScriptRoot\Set-UserMailbox.ps1
. $PSScriptRoot\Set-UserAccount.ps1
. $PSScriptRoot\Set-MailBody.ps1
Import-Module ActiveDirectory

$Domain = 'domain.com'
$Server = 'domain.com'
$SearchBase = 'CN=Users,DC=domain,DC=com'
$DaysInactive = 30
$ExcludedGroups = @('CN=KontaTechniczne,OU=ZABEZPIECZEN,OU=GRUPY,DC=domain,DC=com', 'CN=KontaAdministracyjne,OU=ZABEZPIECZEN,OU=GRUPY,DC=domain,DC=com')
$newOU = 'OU=Automatycznie,OU=Konta zablokowane,DC=domain,DC=com'
$ConnectionUri = 'http://casserver.domain.com/PowerShell/'
$MailFrom = "mail1@domain.com"
$MailToType2 = "mail2@domain.com"
$MailToType1 = "mail3@domain.com"
$MailSubject = "Podsumowanie operacji wylaczania kont domenowych"
$MailSmtpServer = "outlook.domain.com"
$MailAttachmentsType2 = "$PSScriptRoot\Logs\Podsumowanie_$(get-date -f dd-MM-yyyy).csv"
$MailAttachmentsType1 = "$PSScriptRoot\Logs\Podsumowanie_Type1_$(get-date -f dd-MM-yyyy).csv"

$Users = (Get-InactiveUsers -Domain $Domain -Server $Server -SearchBase $SearchBase -DaysInactive $DaysInactive -ExcludedGroups $ExcludedGroups)
if ($Users.count -ne 0)
{
    Set-UserMailbox -Users $Users -ConnectionUri $ConnectionUri
    Set-UserAccount -Users $Users -Server $Server -DaysInactive $DaysInactive -NewOU $newOU

    Send-MailMessage -From $MailFrom -Subject $MailSubject -To $MailToType2 -Attachments $MailAttachmentsType2 -SmtpServer $MailSmtpServer -Encoding UTF8
    Send-MailMessage -From $MailFrom -Subject $MailSubject -To $MailToType1 -Attachments $MailAttachmentsType1 -Body $(Set-MailBody) -SmtpServer $MailSmtpServer -Encoding UTF8
}