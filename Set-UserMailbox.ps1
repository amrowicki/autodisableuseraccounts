. $PSScriptRoot\Out-Log.ps1

function Set-UserMailbox
{
    [CmdletBinding(SupportsShouldProcess = $True)]
    [OutputType([Void])]

    Param(
        [Parameter(Mandatory = $true)]
        [Microsoft.ActiveDirectory.Management.ADUser[]]
        $Users,

        [Parameter(Mandatory = $true)]
        [System.Uri]
        $ConnectionUri
    )
    
    Begin
    {
        if ($PSCmdlet.ShouldProcess($ConnectionUri, "Connecting to $ConnectionUri"))
        {
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Connecting to PSSession: Microsoft.Exchange, $ConnectionUri"
            $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri $ConnectionUri -Authentication Kerberos
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Importing PSSession"
            Import-PSSession $Session | Out-Null
        }
    }
    
    Process
    {
        Foreach ($User in $Users)
        {
            if ($PSCmdlet.ShouldProcess($User, "Setting mailbox"))
            {
                $mailbox = Get-Mailbox $User.samaccountname

                if ($mailbox -ne $null)
                {
                    $emailaddresses = foreach ($emailAddress in $mailbox.emailaddresses)
                    {
                        if (($emailAddress -cmatch "SMTP:") -and -not ($emailAddress -cmatch "SMTP:_"))
                        {
                            $emailAddress -creplace "SMTP:", "SMTP:_"
                        }
                        elseif (($emailAddress -cmatch "smtp:") -and !($emailAddress -cmatch "smtp:_"))
                        {
                            $emailAddress -creplace "smtp:", "smtp:_"
                        }
                        else
                        {
                            $emailAddress
                        }
                    }
                
                    Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Modifying the email address, hide the mailbox from the distribution lists, disabling the policy for setting email address format and setting the limit."
                    try
                    {
                        $mailbox | Set-Mailbox -EmailAddresses $emailaddresses -HiddenFromAddressListsEnabled $true -UseDatabaseQuotaDefaults $false -ProhibitSendReceiveQuota '0 B (0 bytes)' -EmailAddressPolicyEnabled $false -IssueWarningQuota 'unlimited' -ProhibitSendQuota 'unlimited'
                    }
                    catch
                    {
                        Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "ERROR"
                    }
                }
                else
                {
                    Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "The user $($User.samaccountname) does not have an Exchange account"
                }
            }
        }
    }

    End
    {
        if ($PSCmdlet.ShouldProcess($ConnectionUri, "Disconnecting from $ConnectionUri"))
        {
            Out-Log -LogType Type1 -OutputFormat log -Encoding UTF8 -Message "Removing PSSession"
            Remove-PSSession $Session
        }
    }
}