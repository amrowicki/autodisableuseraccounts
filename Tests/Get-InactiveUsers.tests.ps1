. "$(Split-Path $PSScriptRoot -Parent)\Get-InactiveUsers.ps1"

Describe 'Get-InactiveUsers' {
    It 'Retrieves inactive users' {

        . $PSScriptRoot\Classes\TestADUser.ps1
        
        Mock Get-ADUser {
            Param(
                [Parameter()]
                [String]
                $Domain = (Get-ADDomain).DNSRoot,

                [Parameter()]
                [String]
                $Server,

                [Parameter()]
                [String]
                $SearchBase,

                [Parameter()]
                [Int]
                $SearchScope = 2,

                [Parameter()]
                [System.Nullable[Int]]
                $ResultSetSize,

                [Parameter()]
                [Int]
                $DaysInactive = 30,

                [Parameter()]
                [String[]]
                $ExcludedAccounts,

                [Parameter()]
                [String[]]
                $ExcludedGroups,

                [Parameter()]
                [System.Collections.ArrayList]
                $IncludedProperties = @(),

                [Parameter()]
                [String]
                $Filter,

                [Parameter()]
                $Properties
            )

            $users = @(
                [TestADUser]::new('Test User1', 'User1', $false, $($(Get-Date).AddDays(-30).ToFileTime()), @('CN=Group1,OU=Security,OU=Groups,DC=domain,DC=local', 'CN=Group2,OU=Groups,DC=domain,DC=local'), 'CN=Users,DC=domain,DC=local', $($(Get-Date).AddDays(-30).ToFileTime()), $($(Get-Date).AddDays(-14).ToFileTime())),
                [TestADUser]::new('Test User2', 'User2', $false, $($(Get-Date).AddDays(-45).ToFileTime()), @('CN=Group3,OU=Security,OU=Groups,DC=domain,DC=local', 'CN=Group2,OU=Groups,DC=domain,DC=local'), 'CN=Users,DC=domain,DC=local', $($(Get-Date).AddDays(-20).ToFileTime()), $($(Get-Date).AddDays(-41).ToFileTime())),
                [TestADUser]::new('Test User3', 'User3', $false, $($(Get-Date).AddDays(-31).ToFileTime()), @('CN=Group3,OU=Security,OU=Groups,DC=domain,DC=local', 'CN=Group2,OU=Groups,DC=domain,DC=local'), 'CN=Users,DC=domain,DC=local', $($(Get-Date).AddDays(-31).ToFileTime()), $($(Get-Date).AddDays(-31).ToFileTime())),
                [TestADUser]::new('Test User4', 'User4', $false, $($(Get-Date).AddDays(-30).ToFileTime()), @('CN=Group3,OU=Security,OU=Groups,DC=domain,DC=local', 'CN=Group2,OU=Groups,DC=domain,DC=local'), 'CN=Users,DC=domain,DC=local', $($(Get-Date).AddDays(-30).ToFileTime()), $($(Get-Date).AddDays(-30).ToFileTime())),
                [TestADUser]::new('Test User5', 'User5', $false, $($(Get-Date).AddDays(-29).ToFileTime()), @('CN=Group3,OU=Security,OU=Groups,DC=domain,DC=local', 'CN=Group2,OU=Groups,DC=domain,DC=local'), 'CN=Users,DC=domain,DC=local', $($(Get-Date).AddDays(-29).ToFileTime()), $($(Get-Date).AddDays(-29).ToFileTime()))
                [TestADUser]::new('Test User6', 'User6', $false, $($(Get-Date).AddDays(-10).ToFileTime()), @('CN=Group3,OU=Security,OU=Groups,DC=domain,DC=local', 'CN=Group2,OU=Groups,DC=domain,DC=local'), 'CN=Users,DC=domain,DC=local', $($(Get-Date).AddDays(-10).ToFileTime()), $($(Get-Date).AddDays(-10).ToFileTime()))
            )
            Write-Verbose ($users | Out-String) -Verbose
            return ($users | Where-Object lastlogontimestamp -le $lastLogonIntervalLimit)
        }

        $params = @{
            Domain             = 'domain.local'
            Server             = 'domain.local'
            SearchBase         = 'DC=domain,DC=local'
            SearchScope        = 2
            ResultSetSize      = 100
            DaysInactive       = 30
            ExcludedAccounts   = @()
            ExcludedGroups     = @()
            IncludedProperties = @()
        }

        $InactiveUsers = Get-InactiveUsers @params
        Write-Verbose $InactiveUsers.count -Verbose
    }
}