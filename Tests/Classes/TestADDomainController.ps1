Class TestADDomainController : Microsoft.ActiveDirectory.Management.ADDomainController
{
    [String]$Hostname
    [bool]$IsReadonly

    TestADDomainController ([string]$Hostname, [bool]$IsReadonly)
    {
        $this.Hostname = $Hostname
        $this.IsReadonly = $IsReadonly
    }
}