Class TestADUser
{
    [System.Int64]$Lastlogon
    [string]$Name
    [string]$samaccountname
    [System.Boolean]$PasswordNeverExpires
    [System.Int64]$lastlogontimestamp
    [string]$memberof
    [string]$Parent
    [System.Int64]$TrueLastLogon
    [datetime]$TrueLastLogonDateTime
    [datetime]$LastlogonDateTime
    [datetime]$lastlogontimestampDateTime

    TestADUser ([string]$samaccountname)
    {
        $this.samaccountname = $samaccountname
        [DateTime]$MinDate = ([DateTime]::Now).AddYears(-1)
        [DateTime]$MaxDate = [DateTime]::Now
        [System.Random]$Random = [System.Random]::new()
        $this.LastLogon = [System.Int64]([Convert]::ToInt64(($MaxDate.ticks * 1.0 - $MinDate.Ticks * 1.0 ) * $Random.NextDouble() + $MinDate.Ticks * 1.0 ))
    }

    TestADUser ([string]$Name, [string]$SamAccountName, [System.Boolean]$PasswordNeverExpires, [System.Int64]$lastlogontimestamp, [string]$memberof, [string]$Parent, [System.Int64]$LastLogon, [System.Int64]$TrueLastLogon)
    {
        $this.Name = $Name
        $this.SamAccountName = $SamAccountName
        $this.PasswordNeverExpires = $PasswordNeverExpires
        $this.lastlogontimestamp = $lastlogontimestamp
        $this.memberof = $memberof
        $this.Parent = $Parent
        $this.LastLogon = $LastLogon
        $this.TrueLastLogon = $TrueLastLogon

        $this.TrueLastLogonDateTime = [DateTime]::FromFileTime($TrueLastLogon)
        $this.LastlogonDateTime = [DateTime]::FromFileTime($LastLogon)
        $this.lastlogontimestampDateTime = [DateTime]::FromFileTime($lastlogontimestamp)
    }
}