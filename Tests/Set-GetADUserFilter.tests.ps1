. "$(Split-Path $PSScriptRoot -Parent)\Set-GetADUserFilter.ps1"

Describe 'Set-GetADUserFilter' {
    $lastLogonIntervalLimitTestCase1 = $(Get-Date).AddDays(-30).ToFileTime()
    $lastLogonIntervalLimitTestCase2 = $(Get-Date).AddDays(-100).ToFileTime()
    $lastLogonIntervalLimitTestCase3 = $(Get-Date).AddDays(-31).ToFileTime()
    $lastLogonIntervalLimitTestCase4 = $(Get-Date).AddDays(-20).ToFileTime()
    $lastLogonIntervalLimitTestCase5 = $(Get-Date).AddDays(-1).ToFileTime()
    It "Given parameters lastLogonIntervalLimit:<lastLogonIntervalLimit>, ExcludedAccounts:<ExcludedAccounts>, ExcludedGroups:<ExcludedGroups>. Should return <Expected>" -TestCases @(
        @{lastLogonIntervalLimit = $lastLogonIntervalLimitTestCase1; ExcludedAccounts = @(); ExcludedGroups = @();
            Expected = "(lastlogontimestamp -le $lastLogonIntervalLimitTestCase1 -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimitTestCase1)).datetime)'"
        }
        @{lastLogonIntervalLimit = $lastLogonIntervalLimitTestCase2; ExcludedAccounts = @('Acccount1', 'Acccount2', 'Acccount3'); ExcludedGroups = @();
            Expected = "(lastlogontimestamp -le $lastLogonIntervalLimitTestCase2 -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimitTestCase2)).datetime)' -and samAccountname -ne 'Acccount1' -and samAccountname -ne 'Acccount2' -and samAccountname -ne 'Acccount3'"
        }
        @{lastLogonIntervalLimit = $lastLogonIntervalLimitTestCase3; ExcludedAccounts = @('Acccount4'); ExcludedGroups = @();
            Expected = "(lastlogontimestamp -le $lastLogonIntervalLimitTestCase3 -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimitTestCase3)).datetime)' -and samAccountname -ne 'Acccount4'"
        }
        @{lastLogonIntervalLimit = $lastLogonIntervalLimitTestCase4; ExcludedAccounts = @('Acccount5'); ExcludedGroups = @('Group1');
            Expected = "(lastlogontimestamp -le $lastLogonIntervalLimitTestCase4 -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimitTestCase4)).datetime)' -and samAccountname -ne 'Acccount5' -and memberof -notlike 'Group1'"
        }
        @{lastLogonIntervalLimit = $lastLogonIntervalLimitTestCase5; ExcludedAccounts = @('Acccount6'); ExcludedGroups = @('Group2', 'Group3');
            Expected = "(lastlogontimestamp -le $lastLogonIntervalLimitTestCase5 -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimitTestCase5)).datetime)' -and samAccountname -ne 'Acccount6' -and memberof -notlike 'Group2' -and memberof -notlike 'Group3'"
        }
    ) {
        param($lastLogonIntervalLimit, $ExcludedAccounts, $ExcludedGroups, $Expected)

        $filter = Set-GetADUserFilter -lastLogonIntervalLimit $lastLogonIntervalLimit -ExcludedAccounts $ExcludedAccounts -ExcludedGroups $ExcludedGroups
        $filter | Should -BeOfType String
        $filter | Should -Be $Expected
    }
}