. "$(Split-Path $PSScriptRoot -Parent)\Set-GetADUserProperties.ps1"

Describe 'Set-GetADUserProperties' {

    $lastLogonIntervalLimitTestCase1 = $(Get-Date).AddDays(-30).ToFileTime()

    It "Given only mandatory parameters, returns default parameters and default filter" -TestCases @(
        @{SearchScope              = 2
            SearchBase             = 'DC=comain,DC=com'
            Server                 = 'domain.com'
            lastLogonIntervalLimit = $lastLogonIntervalLimitTestCase1
            ExpectedFilter         =  "(lastlogontimestamp -le $lastLogonIntervalLimitTestCase1 -or -not lastlogontimestamp -like '*') -and enabled -eq 'True' -and Created -le '$(([DateTime]::FromFileTime($lastLogonIntervalLimitTestCase1)).datetime)'"
            PropertiesCount        = 5
        }
    ) {
        param($SearchScope, $SearchBase, $Server, $lastLogonIntervalLimit, $ExpectedFilter, $PropertiesCount)

        $Properties = Set-GetADUserProperties -SearchScope $SearchScope -SearchBase $SearchBase -Server $Server -lastLogonIntervalLimit $lastLogonIntervalLimit

        $Properties | Should -BeOfType System.Collections.Hashtable
        $Properties.Count | Should -BeExactly $PropertiesCount
        $Properties.SearchBase | Should -Match $SearchBase

        $Properties.Properties.Count | Should -BeExactly 6
        $Properties.Properties | Should -Contain 'Name'
        $Properties.Properties | Should -Contain 'samAccountName'
        $Properties.Properties | Should -Contain 'PasswordNeverExpires'
        $Properties.Properties | Should -Contain 'lastlogontimestamp'
        $Properties.Properties | Should -Contain 'memberof'
        $Properties.Properties | Should -Contain 'Created'
        
        $Properties.Filter | Should -Be $ExpectedFilter
        $Properties.SearchScope | Should -BeExactly $SearchScope

        {$Properties} | Should -Not -Throw
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'Parent' | Should -Be $false
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'TrueLastLogonTimestamp' | Should -Be $True
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'TrueLastLogonTimestampDateTime' | Should -Be $True
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'LastLogonTimestampDateTime' | Should -Be $True
    }
    
    BeforeEach {
        if ((Get-TypeData Microsoft.ActiveDirectory.Management.ADUser) -ne $null)
        {
            Remove-TypeData Microsoft.ActiveDirectory.Management.ADUser
            Import-Module ActiveDirectory -Force
        }
    }

    AfterAll {
        if ((Get-TypeData Microsoft.ActiveDirectory.Management.ADUser) -ne $null)
        {
            Remove-TypeData Microsoft.ActiveDirectory.Management.ADUser
            Import-Module ActiveDirectory -Force
        }
    }
}