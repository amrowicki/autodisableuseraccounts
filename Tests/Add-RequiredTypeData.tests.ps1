. "$(Split-Path $PSScriptRoot -Parent)\Add-RequiredTypeData.ps1"

Describe 'Add-RequiredTypeData' {

    It "Updates Microsoft.ActiveDirectory.Management.ADUser for Parent <ExpectedParent>, TrueLastLogonTimestamp <ExpectedTrueLastLogonTimestamp>, TrueLastLogonTimestampDateTime <ExpectedTrueLastLogonTimestampDateTime> and LastLogonTimestampDateTime <ExpectedLastLogonTimestampDateTime>" -TestCases @(
        @{Params = @{IncludeParent = $false; Server = 'domain.com'}; ExpectedParent = $false; ExpectedTrueLastLogonTimestamp = $true; ExpectedTrueLastLogonTimestampDateTime = $true; ExpectedLastLogonTimestampDateTime = $true}
        @{Params = @{IncludeParent = $true; Server = 'domain.com'}; ExpectedParent = $true; ExpectedTrueLastLogonTimestamp = $true; ExpectedTrueLastLogonTimestampDateTime = $true; ExpectedLastLogonTimestampDateTime = $true}
    ) {
        param($Params, $ExpectedParent, $ExpectedTrueLastLogonTimestamp, $ExpectedTrueLastLogonTimestampDateTime, $ExpectedLastLogonTimestampDateTime)

        {Add-RequiredTypeData @Params} | Should -Not -Throw
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'Parent' | Should -Be $ExpectedParent
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'TrueLastLogonTimestamp' | Should -Be $ExpectedTrueLastLogonTimestamp
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'TrueLastLogonTimestampDateTime' | Should -Be $ExpectedTrueLastLogonTimestampDateTime
        (Get-TypeData -TypeName Microsoft.ActiveDirectory.Management.ADUser).Members.Keys -contains 'LastLogonTimestampDateTime' | Should -Be $ExpectedLastLogonTimestampDateTime

        $TestUser = Get-ADUser -filter "lastlogontimestamp -gt 0" -ResultSetSize 1
        $TestUser.TrueLastLogonTimestamp | Should -BeOfType System.Int64
        $TestUser.TrueLastLogonTimestampDateTime | Should -BeOfType datetime
        $TestUser.LastLogonTimestampDateTime | Should -BeOfType datetime

        if($ExpectedParent){
            $TestUser.Parent | Should -BeOfType String
            $TestUser.Parent | Should -Match ([regex]::new('^((OU|CN)=[a-z]+,)*((,DC=|DC=)[a-zA-Z0-9]+)*$'))
        }
    }

    BeforeEach {
        if ((Get-TypeData Microsoft.ActiveDirectory.Management.ADUser) -ne $null)
        {
            Remove-TypeData Microsoft.ActiveDirectory.Management.ADUser
            Import-Module ActiveDirectory -Force
        }
    }
}