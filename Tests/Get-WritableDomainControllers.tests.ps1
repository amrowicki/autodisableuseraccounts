. "$(Split-Path $PSScriptRoot -Parent)\Get-WritableDomainControllers.ps1"

Describe 'Get-WritableDomainControllers' {
    It 'Retrieves writable domain controllers' -TestCases @(
        @{Server = 'domain.com'; ExpectedCount = (Get-ADDomainController -Filter * -Server domain.com | Where-Object IsReadonly -eq $false).count}
        @{Server = 'domain2.com'; ExpectedCount = (Get-ADDomainController -Filter * -Server domain2.com | Where-Object IsReadonly -eq $false).count}
    ) {
        param($Server, $ExpectedCount)

        (Get-WritableDomainControllers -Server $Server).count | Should -BeExactly $ExpectedCount
    }

    It 'Retrieves writable domain controllers - mocked Get-ADDomainController' -TestCases @(
        @{Server = 'dc01.domain.local'; DomainControllers = @(
                @{Hostname = 'dc01.domain.local'; IsReadonly = $false}); ExpectedCount = 1
        }
        @{Server = 'dc02.domain.local'; DomainControllers = @(
                @{Hostname = 'dc01.domain.local'; IsReadonly = $false}
                @{Hostname = 'dc02.domain.local'; IsReadonly = $false}
                @{Hostname = 'dc03.domain.local'; IsReadonly = $True}); ExpectedCount = 2
        }
    ) {
        param($Server, $DomainControllers, $ExpectedCount)

        . $PSScriptRoot\Classes\TestADDomainController.ps1

        Mock Get-ADDomainController {
            $return = foreach ($DomainController in $DomainControllers)
            {
                [TestADDomainController]::new($DomainController.Hostname, $DomainController.IsReadonly)
            }
            return $return
        }

        (Get-WritableDomainControllers -Server $Server).count | Should -BeExactly $ExpectedCount
    }
}