. "$(Split-Path $PSScriptRoot -Parent)\Get-LastLogon.ps1"

Describe 'Get-LastLogon' {
    It 'Retrieves last logon time' -TestCases @(
        @{SamAccountName = 'account1'; Server = 'domain.com'}
        @{SamAccountName = 'account2'; Server = 'domain.com'}
    ) {
        param($SamAccountName, $Server) 

        $TrueLastLogon = Get-LastLogon -SamAccountName $SamAccountName -Server $Server
        $TrueLastLogon[0] | Should -BeOfType [System.Int64]
        $TrueLastLogon[1] | Should -BeOfType [Datetime]
    }

    It 'Retrieves last logon time - mocked Get-ADUser' -TestCases @(
        @{SamAccountName = 'account1'; Server = 'domain.com'}
        @{SamAccountName = 'account2'; Server = 'domain.com'}
    ) {
        param($SamAccountName, $Server)

        . $PSScriptRoot\Classes\TestADUser.ps1

        Mock Get-ADUser {
            [TestADUser]::new($SamAccountName)
        }

        $TrueLastLogon = Get-LastLogon -SamAccountName $SamAccountName -Server $Server
        $TrueLastLogon[0] | Should -BeOfType [System.Int64]
        $TrueLastLogon[1] | Should -BeOfType [Datetime]
    }

    It 'Retrieves last logon time - mocked Get-ADUser and Get-ADDomainController' -TestCases @(
        @{SamAccountName = 'User2'; Server = 'dc01.domain.local'; DomainControllers = @(
                @{Hostname = 'dc01.domain.local'; IsReadonly = $false})
        }
        @{SamAccountName = 'User2'; Server = 'dc02.domain.local'; DomainControllers = @(
                @{Hostname = 'dc02.domain.local'; IsReadonly = $false}
                @{Hostname = 'dc01.domain.local'; IsReadonly = $true})
        }
    ) {
        param($SamAccountName, $Server, $DomainControllers)

        . $PSScriptRoot\Classes\TestADUser.ps1
        . $PSScriptRoot\Classes\TestADDomainController.ps1

        Mock Get-ADDomainController {
            $return = foreach ($DomainController in $DomainControllers)
            {
                [TestADDomainController]::new($DomainController.Hostname, $DomainController.IsReadonly)
            }
            return $return
        }

        Mock Get-ADUser {
            [TestADUser]::new($SamAccountName)
        }

        $TrueLastLogon = Get-LastLogon -SamAccountName $SamAccountName -Server $Server
        $TrueLastLogon[0] | Should -BeOfType [System.Int64]
        $TrueLastLogon[1] | Should -BeOfType [Datetime]
    }
}